# Varnam Language Pack

Note: This repo is based on [this repo](https://gitlab.com/indicproject/varnam/ml), general changes from there should be merged here too.

## Setup

* git LFS is required
* `git submodule update --init`
* Make VST and packs:
```
cd schemes
./build_libvarnam.sh
./build_all_schemes.sh
./build_all_packs.sh
```
* Update assets from the submodule :
```
python3 update-assets.py
```
* Open project in Android Studio and build
