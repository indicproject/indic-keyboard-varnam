/**
 * Copyright Subin Siby, <mail at subinsb.com>
 * Licensed under AGPL-3.0-only
 */

package org.smc.inputmethod.indic.varnam;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.varnamproject.govarnam.SchemeDetails;
import com.varnamproject.govarnam.Suggestion;
import com.varnamproject.govarnam.Varnam;

import org.xml.sax.InputSource;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.zip.GZIPInputStream;

public class VarnamService extends Service {
    public final static int MSG_SETUP = 0;
    public final static int MSG_INIT = 1;
    public final static int MSG_TRANSLITERATE = 2;
    public final static int MSG_CANCEL = 3;
    public final static int MSG_LEARN = 4;
    public final static int MSG_UNLEARN = 5;
    public final static int MSG_SET_DICTIONARY_SUGGESTIONS_LIMIT = 6;
    public final static int MSG_SET_TOKENIZER_SUGGESTIONS_LIMIT = 7;

    public final static String ERROR_VST_MISSING = "vst-missing";

    private String schemeID;
    private Varnam varnam;

    @Override
    public IBinder onBind(Intent intent) {
        Messenger mMessenger = new Messenger(new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(@NonNull Message msg) {
                try {
                    Bundle data = (Bundle) msg.obj;
                    if (msg.what == MSG_SETUP) {
                        schemeID = data.getString("schemeID");
                        setupScheme(getApplicationContext(), schemeID, false);

                        Bundle replyData = new Bundle();
                        replyData.putString("result", "ok");
                        msg.replyTo.send(Message.obtain(null, msg.what, replyData));
                    } else if (msg.what == MSG_INIT) {
                        schemeID = data.getString("schemeID");
                        varnam = makeVarnam(getApplicationContext(), schemeID);

                        updateImports(getApplicationContext(), schemeID);

                        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("varnam", Context.MODE_PRIVATE);
                        boolean learn = prefs.getBoolean("varnam_learn", true);

                        Bundle replyData = new Bundle();
                        replyData.putBoolean("setting_learn", learn);
                        replyData.putString("result", "ok");
                        msg.replyTo.send(Message.obtain(null, msg.what, replyData));
                    } else if (msg.what == MSG_SET_DICTIONARY_SUGGESTIONS_LIMIT) {
                        varnam.setDictionarySuggestionsLimit(data.getInt("limit"));
                    } else if (msg.what == MSG_SET_TOKENIZER_SUGGESTIONS_LIMIT) {
                        varnam.setTokenizerSuggestionsLimit(data.getInt("limit"));
                    } else if (msg.what == MSG_TRANSLITERATE) {
                        int id = data.getInt("id");
                        String input = data.getString("input");

                        Suggestion[] result = varnam.transliterate(id, input);

                        Bundle replyData = new Bundle();
                        replyData.setClassLoader(Suggestion.class.getClassLoader());
                        replyData.putParcelableArray("result", result);
                        msg.replyTo.send(Message.obtain(null, msg.what, replyData));
                    } else if (msg.what == MSG_CANCEL) {
                        int id = data.getInt("id");
                        varnam.cancel(id);
                    } else if (msg.what == MSG_LEARN) {
                        String input = data.getString("input");
                        varnam.learn(input);
                    } else if (msg.what == MSG_UNLEARN) {
                        String input = data.getString("input");
                        varnam.unlearn(input);
                    }
                } catch (Exception e) {
                    Bundle replyData = new Bundle();
                    replyData.putString("error", e.getMessage());
                    try {
                        if (msg.replyTo != null) {
                            msg.replyTo.send(Message.obtain(null, msg.what, replyData));
                        }
                    } catch (RemoteException remoteException) {
                        remoteException.printStackTrace();
                    }
                    e.printStackTrace();
                }
            }
        });
        Log.d("varnam", "service connected");
        return mMessenger.getBinder();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        if (varnam != null) {
            varnam.close();
            varnam = null;
        }
        Log.d("varnam", "service disconnected for " + schemeID);
        return true;
    }

    public static Varnam makeVarnam(Context context, String schemeID) throws Exception {
        Varnam varnam;

        File vstFile = new File(getVarnamVSTFolder(context), schemeID + ".vst");
        File learningsFile = new File(getVarnamFolder(context, schemeID), schemeID + ".vst.learnings");

        if (!vstFile.exists()) {
            throw new Exception(ERROR_VST_MISSING);
        }

        varnam = new Varnam(vstFile.getAbsolutePath(), learningsFile.getAbsolutePath());
        return varnam;
    }

    public static File getVarnamVSTFolder(Context context) {
        return getVarnamFolder(context, "schemes");
    }

    public static File getVarnamFolder(Context context, String subFolder) {
        // More about storage: https://stackoverflow.com/a/29404440/1372424
        File varnamFolder = new File(context.getExternalFilesDir(null).getPath() + "/varnam", subFolder);

        if (!varnamFolder.exists()) {
            varnamFolder.mkdirs();
        }
        return varnamFolder;
    }

    private static SchemeDetails makeSchemeDetails(String id, String name) {
        SchemeDetails sd = new SchemeDetails();
        sd.Identifier = id;
        sd.DisplayName = name;
        return sd;
    }

    public static final HashMap<String, SchemeDetails> schemes = new HashMap<String, SchemeDetails>() {{
//        put("bn", new Scheme("bn", "bn", "Bangla", "", "0.1", ""));
//        put("kn", new Scheme("kn", "kn", "Kannada", "", "0.1", ""));
//        put("gu", new Scheme("gu", "gu", "Gujarati", "", "0.1", ""));
//        put("hi", new Scheme("hi", "hi", "Hindi", "", "0.1", ""));
        put("ml", makeSchemeDetails("ml", "Malayalam"));
//        put("ta", new Scheme("ta", "ta", "Tamil", "", "0.1", ""));
//        put("te", new Scheme("te", "te", "Telugu", "", "0.1", ""));
    }};

    public static HashMap<String, SchemeDetails> getInstalledSchemes(Context context) {
        HashMap<String, SchemeDetails> installedSchemes = new HashMap<>();

        Varnam.setVSTLookupDir(getVarnamVSTFolder(context).getAbsolutePath());
        SchemeDetails[] sdArr = Varnam.getAllSchemeDetails();
        for (SchemeDetails sd: sdArr) {
            installedSchemes.put(sd.Identifier, sd);
        }
        return installedSchemes;
    }

    public static boolean isSchemeInstalled(String schemeID, Context context) {
        return getInstalledSchemes(context).containsKey(schemeID);
    }

    // Unpacks a scheme from assets, import words.
    // Firts install setup
    public static void setupScheme(Context context, String schemeID, boolean force) {
        File vstFile = new File(VarnamService.getVarnamVSTFolder(context), schemeID + ".vst");

        if (!vstFile.exists() || force) {
            try {
                InputStream is = context.getAssets().open(schemeID + ".vst");
                int size = is.available();
                byte[] buffer = new byte[size];
                is.read(buffer);
                is.close();

                FileOutputStream fos = new FileOutputStream(vstFile);
                fos.write(buffer);
                fos.close();
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }

        importDictionaryFromAssets(context, schemeID, force);
    }

    // Imports words from VLFs packed in this app assets
    public static void importDictionaryFromAssets(Context context, String schemeID, boolean force) {
        SharedPreferences settings = context.getSharedPreferences("import", MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();

        String lastImportVersion = settings.getString(schemeID, "0.0.0");

        // Import
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            String version = pInfo.versionName;

            if (lastImportVersion.equals(version) && !force) {
                Log.d("varnam", "version matched. not running import");
                return;
            }

            Varnam varnam = makeVarnam(context, schemeID);

            AssetManager am = context.getAssets();
            String fileList[] = am.list("");

            for (String fileName : fileList) {
                if (fileName.endsWith(".gzip") && fileName.startsWith(schemeID + "-")) {
                    Log.d("varnam", "Importing " + fileName);
                    InputStream in = am.open(fileName);
                    File outFile = File.createTempFile("varnam", "", context.getCacheDir());
                    FileUtil.unzip(in, outFile);

                    varnam.importFromFile(outFile.getAbsolutePath());
                    outFile.delete();
                }
            }

            /**
             * We store the app version code.
             * We check if this matches with current app version, if not
             * update process is again ran.
             */
            editor.putString(schemeID, version);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Checks if app updated. If so, import new words in background
    public static void updateImports(final Context context, final String schemeID) {
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            String version = pInfo.versionName;

            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    importDictionaryFromAssets(context, schemeID, false);
                }
            };
            Thread thread = new Thread(runnable);
            thread.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
