/*
 * Copyright (C) 2020, Subin Siby <mail@subinsb.com>
 * Licensed under AGPL-3.0-only
 */

package org.smc.inputmethod.indic.varnam;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import androidx.preference.PreferenceCategory;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceScreen;

import com.varnamproject.govarnam.SchemeDetails;

import java.util.HashMap;

/**
 * "Varnam" language settings sub screen.
 * Allows user to configure varnam languages here
 */
public final class VarnamSettingsFragment extends PreferenceFragmentCompat {
    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.prefs_screen_varnam);
        setupVarnamLangs();
    }

    private void setupVarnamLangs() {
        Context context = getPreferenceManager().getContext();
        androidx.preference.PreferenceScreen preferenceScreen = this.getPreferenceScreen();

        HashMap<String, SchemeDetails> installedLanguages = VarnamService.getInstalledSchemes(context);

        // Available languages (VST file installed to use)
        PreferenceCategory enabledPreferenceCategory = new PreferenceCategory(preferenceScreen.getContext());
        enabledPreferenceCategory.setTitle(R.string.pref_varnam_category_enabled);
        enabledPreferenceCategory.setIconSpaceReserved(false);
        preferenceScreen.addPreference(enabledPreferenceCategory);

        for (HashMap.Entry<String, SchemeDetails> entry : installedLanguages.entrySet()) {
            String schemeID = entry.getKey();
            SchemeDetails scheme = entry.getValue();

            PreferenceScreen screen = getPreferenceManager().createPreferenceScreen(context);
            screen.setTitle(scheme.DisplayName);
            screen.setFragment("org.smc.inputmethod.indic.varnam.VarnamSettingsLangFragment");
            screen.setIconSpaceReserved(false);
            screen.getExtras().putString("schemeID", schemeID);

            enabledPreferenceCategory.addPreference(screen);
        }

        // Languages
        PreferenceCategory disabledPreferenceCategory = new PreferenceCategory(preferenceScreen.getContext());
        disabledPreferenceCategory.setTitle(R.string.pref_varnam_category_disabled);
        disabledPreferenceCategory.setIconSpaceReserved(false);
        preferenceScreen.addPreference(disabledPreferenceCategory);

        for (HashMap.Entry<String, SchemeDetails> entry : VarnamService.schemes.entrySet()) {
            String schemeID = entry.getKey();

            if (installedLanguages.containsKey(schemeID)) {
                continue;
            }

            SchemeDetails scheme = entry.getValue();

            PreferenceScreen screen = getPreferenceManager().createPreferenceScreen(context);
            screen.setTitle(scheme.DisplayName);
            screen.setFragment("org.smc.inputmethod.indic.varnam.VarnamSettingsLangFragment");
            screen.setIconSpaceReserved(false);
            screen.getExtras().putString("schemeID", schemeID);

            disabledPreferenceCategory.addPreference(screen);
        }
    }
}
