package org.smc.inputmethod.indic.varnam;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;

import androidx.preference.Preference;
import androidx.preference.PreferenceViewHolder;

public class VarnamSettingsRLWItem extends Preference {
    public VarnamSettingsRLWItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public VarnamSettingsRLWItem(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public VarnamSettingsRLWItem(Context context) {
        super(context);
    }

    private View.OnClickListener clickListener;
    public void setOnUnlearnClickListener(View.OnClickListener listener) {
        this.clickListener = listener;
    }

    @Override
    public void onBindViewHolder(final PreferenceViewHolder holder) {
        super.onBindViewHolder(holder);
        Button button = holder.itemView.findViewById(R.id.varnam_rlw_unlearn);
        if (button != null){
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickListener.onClick(v);
                }
            });
        }
    }
}
