/*
 * Copyright (C) 2020, Subin Siby <mail@subinsb.com>
 * Licensed under AGPL-3.0-only
 */

package org.smc.inputmethod.indic.varnam;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.opengl.Visibility;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.documentfile.provider.DocumentFile;
import androidx.preference.Preference;
import androidx.preference.PreferenceCategory;
import androidx.preference.PreferenceFragmentCompat;

import com.varnamproject.govarnam.LearnStatus;
import com.varnamproject.govarnam.SchemeDetails;
import com.varnamproject.govarnam.Varnam;

import org.xml.sax.InputSource;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.zip.GZIPInputStream;

/**
 * "Varnam" language settings sub screen.
 *
 * This settings sub screen handles the following varnam preferences.
 * - TODO Language download
 * - Import words
 */
public final class VarnamSettingsLangFragment extends PreferenceFragmentCompat {
    private static String TAG = "VarnamSettings";

    private static int PICK_VARNAM_PACK_APP = 1;
    private static int PICK_VARNAM_CORPUS_FILE = 2;
    private static int PICK_VARNAM_VLF = 3;
    private static int PICK_VARNAM_EXPORT_DIR = 4;

    private String schemeID;
    private SchemeDetails scheme;
    private boolean installed = false; // Whether VST is available to use

    private VarnamSettingsProcessInfo processInfo;

    private Thread learnThread;

    @Override
    public void onCreatePreferences(@Nullable Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.prefs_screen_varnam_lang);

        processInfo = new VarnamSettingsProcessInfo(getContext());
        getPreferenceScreen().addPreference(processInfo);

        schemeID = getArguments().getString("schemeID");
        scheme = VarnamService.schemes.get(schemeID);
        installed = VarnamService.isSchemeInstalled(schemeID, getContext());

        PreferenceCategory packCategory = findPreference("pref_varnam_category_scheme");
        packCategory.setTitle(getString(R.string.pref_varnam_category_scheme, scheme.DisplayName));

        setups();

        if (savedInstanceState != null) {
            int statusVisibility = savedInstanceState.getInt("statusVisibility");
            if (processInfo != null) {
                processInfo.setVisibility(statusVisibility);
            }
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        // Save UI state changes to the savedInstanceState.
        // This bundle will be passed to onCreate if the process is
        // killed and restarted.
        savedInstanceState.putInt("statusVisibility", processInfo.getVisibility());
    }

    @Override
    public void onStop() {
        super.onStop();
        if (learnThread != null) {
            learnThread.interrupt();
            learnThread = null;
            processInfo.logTextView.setText("");
            processInfo.setVisibility(View.GONE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        if (requestCode == PICK_VARNAM_CORPUS_FILE && resultCode == Activity.RESULT_OK) {
            if (resultData != null) {
                final Uri uri = resultData.getData();

                processInfo.setVisibility(View.VISIBLE);
                processInfo.progressBar.setIndeterminate(true);
                processInfo.logTextView.setText("");
                log(getString(R.string.varnam_import_copy, uri.getPath()));

                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        learnFromFile(uri);
                    }
                };
                learnThread = new Thread(runnable);
                learnThread.start();
            }
        } else if (requestCode == PICK_VARNAM_VLF && resultCode == Activity.RESULT_OK) {
            if (resultData != null) {
                final Uri uri = resultData.getData();

                processInfo.setVisibility(View.VISIBLE);
                processInfo.progressBar.setIndeterminate(true);
                processInfo.logTextView.setText("");
                log(getString(R.string.varnam_import_copy, uri.getPath()));

                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        importFromFile(uri, 1, 1);
                    }
                };
                learnThread = new Thread(runnable);
                learnThread.start();
            }
        } else if (requestCode == PICK_VARNAM_EXPORT_DIR && resultCode == Activity.RESULT_OK) {
            if (resultData != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                final Uri uri = resultData.getData();

                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        exportVLF(uri);
                    }
                };
                learnThread = new Thread(runnable);
                learnThread.start();
            }
        } else {
            Log.e(TAG, "Activity returned fail");
        }
    }

    private void log(String msg) {
        processInfo.logTextView.append(msg + "\n");
    }

    private void learnFromFile(Uri uri) {
        try {
            // We're copying file to avoid asking external storage permission to read file
            // TODO better/alternate way to do this ?
            final String path = copyUriAndGetPath(uri);
            Varnam varnam = VarnamService.makeVarnam(getContext(), scheme.Identifier);

            processInfo.progressBar.post(new Runnable() {
                @Override
                public void run() {
                    processInfo.progressBar.setIndeterminate(true);
                    log(getString(R.string.varnam_import_running, 1, 1));
                }
            });

            Log.d("varnam", "Learn file : " + path);
            final LearnStatus status = varnam.learnFromFile(path);

            processInfo.progressBar.post(new Runnable() {
                @Override
                public void run() {
                    log("Removing cached file.");
                    new File(path).delete();

                    processInfo.progressBar.setIndeterminate(false);
                    processInfo.progressBar.setProgress(100);

                    log(getString(R.string.varnam_import_completed));
                    log(getString(R.string.varnam_import_status, status.TotalWords, status.FailedWords));
                }
            });
        } catch (Exception e) {
            Log.e("VarnamLearn", e.getMessage());

            final Exception err = e;
            processInfo.progressBar.post(new Runnable() {
                @Override
                public void run() {
                    processInfo.progressBar.setIndeterminate(false);
                    processInfo.progressBar.setProgress(50);
                    log(err.getMessage());
                }
            });
        }
    }

    /**
     * Import a pack/trained/varnam exported file
     * @param uri
     */
    private void importFromFile(Uri uri, final int index, final int total) {
        try {
            // We're copying file to avoid asking external storage permission to read file
            // TODO better/alternate way to do this ?
            final String path = copyUriAndGetPath(uri);
            Varnam varnam = VarnamService.makeVarnam(getContext(), scheme.Identifier);

            if (isGZipFile(path)) {
                // uncompress to the same file
                InputStream in = getActivity().getContentResolver().openInputStream(uri);
                unzip(in, path);
            }

            processInfo.progressBar.post(new Runnable() {
                @Override
                public void run() {
                    processInfo.progressBar.setIndeterminate(true);
                    log(getString(R.string.varnam_import_running, index, total));
                }
            });

            Log.d("varnam", "VLF file : " + path);
            varnam.importFromFile(path);

            processInfo.progressBar.post(new Runnable() {
                @Override
                public void run() {
                    log("Removing cached file.");
                    new File(path).delete();

                    processInfo.progressBar.setIndeterminate(false);
                    processInfo.progressBar.setProgress(100);
                    log(getString(R.string.varnam_import_completed));
                }
            });
        } catch (Exception e) {
            Log.e("VarnamLearn", e.getMessage());

            final Exception err = e;
            processInfo.progressBar.post(new Runnable() {
                @Override
                public void run() {
                    processInfo.progressBar.setIndeterminate(false);
                    processInfo.progressBar.setProgress(50);
                    log(err.getMessage());
                }
            });
        }
    }

    // path is filename included
    private void exportVLF(String path) {
        try {
            Varnam varnam = VarnamService.makeVarnam(getContext(), scheme.Identifier);

            processInfo.progressBar.post(new Runnable() {
                @Override
                public void run() {
                    processInfo.progressBar.setIndeterminate(true);
                    log(getString(R.string.varnam_export_running));
                }
            });

            Log.d("varnam", "Export directory: " + path);
            varnam.export(path, 30000);
        } catch (Exception e) {
            Log.e("VarnamExport", e.getMessage());

            final Exception err = e;
            processInfo.progressBar.post(new Runnable() {
                @Override
                public void run() {
                    processInfo.progressBar.setIndeterminate(false);
                    processInfo.progressBar.setProgress(50);
                    log(err.getMessage());
                }
            });
        }
    }

    private void exportVLF(Uri uri) {
        try {
            File dir;
            String tempLogPath;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                dir = File.createTempFile("varnam", "", getActivity().getCacheDir()).getParentFile();
                tempLogPath = uri.getPath();
            } else {
                String filePath = FileUtil.getFullPathFromTreeUri(uri, getContext());
                dir = new File(filePath).getParentFile();
                tempLogPath = dir.getAbsolutePath();
            }

            final String logPath = tempLogPath;

            processInfo.progressBar.post(new Runnable() {
                @Override
                public void run() {
                    processInfo.setVisibility(View.VISIBLE);
                    processInfo.progressBar.setIndeterminate(true);
                    processInfo.logTextView.setText("");
                    log(getString(R.string.varnam_export_begin, logPath));
                }
            });

            exportVLF(new File(dir, "varnam-ik-learnings").getAbsolutePath());

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                File[] exportFiles = dir.listFiles(new FileFilter() {
                    @Override
                    public boolean accept(File file) {
                        return file.getName().startsWith("varnam-ik-learnings");
                    }
                });
                for (File exportFile: exportFiles) {
                    DocumentFile targetDir = DocumentFile.fromTreeUri(getContext(), uri);
                    DocumentFile targetExportFile = targetDir.createFile("text/vlf", exportFile.getName());
                    OutputStream targetExportFileStream = getContext().getContentResolver().openOutputStream(targetExportFile.getUri());
                    copyFile(new FileInputStream(exportFile), targetExportFileStream);
                }
            }

            processInfo.progressBar.post(new Runnable() {
                @Override
                public void run() {
                    processInfo.progressBar.setIndeterminate(false);
                    processInfo.progressBar.setProgress(100);
                    log(getString(R.string.varnam_export_completed));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();

            final Exception err = e;
            processInfo.progressBar.post(new Runnable() {
                @Override
                public void run() {
                    processInfo.progressBar.setIndeterminate(false);
                    processInfo.progressBar.setProgress(50);
                    log(err.getMessage());
                }
            });
        }
    }

    private void setups() {
        setupVarnamEnableScheme();
        setupVarnamRLW();
        setupVarnamImportFile();
        setupVarnamImportVLF();
        setupVarnamExportVLF();
    }

    /**
     * Shows a Install Language pack button at the top if lang not available
     * Otherwise, shows an update button
     * Thanks user207064 https://stackoverflow.com/a/31586010/1372424
     * Licensed under CC-BY-SA 3.0
     */
    private void setupVarnamEnableScheme() {
        final Context context = getContext();

        Preference enableSchemeButton = findPreference("pref_varnam_enable_scheme");
        Preference rerunScehemSetupButton = findPreference("pref_varnam_rerun_scheme_setup");

        Preference.OnPreferenceClickListener setupScheme = new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                processInfo.setVisibility(View.VISIBLE);
                processInfo.progressBar.setIndeterminate(true);
                processInfo.logTextView.setText("");
                log(getString(R.string.varnam_setup_begin, scheme.DisplayName));

                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        VarnamService.setupScheme(context, schemeID, installed);
                        processInfo.progressBar.post(new Runnable() {
                             @Override
                             public void run() {
                                 log(getString(R.string.varnam_setup_finished, scheme.DisplayName));
                                 processInfo.progressBar.setIndeterminate(false);
                                 processInfo.progressBar.setProgress(100);
                                 setups();
                             }
                         });
                    }
                });
                thread.start();
                return true;
            }
        };

        if (installed) {
            enableSchemeButton.setVisible(false);
            rerunScehemSetupButton.setVisible(true);
            rerunScehemSetupButton.setOnPreferenceClickListener(setupScheme);
        } else {
            enableSchemeButton.setVisible(true);
            rerunScehemSetupButton.setVisible(false);
            enableSchemeButton.setOnPreferenceClickListener(setupScheme);
        }
    }


    private void setupVarnamRLW() {
        Preference rlw = findPreference("pref_varnam_rlw");

        rlw.setEnabled(installed);
        if (!installed) {
            return;
        }

        rlw.setFragment("org.smc.inputmethod.indic.varnam.VarnamSettingsRLWFragment");
        rlw.setIconSpaceReserved(false);
        rlw.getExtras().putString("schemeID", schemeID);
    }

    private void setupVarnamImportFile() {
        Preference filePicker = findPreference("pref_varnam_import_file");

        filePicker.setEnabled(installed);
        if (!installed) {
            return;
        }

        filePicker.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Intent intent = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                    intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                    intent.setType("*/*");
                } else {
                    intent = new Intent(Intent.ACTION_GET_CONTENT);
                    // TODO might not work in some file explorers ?
                    // https://stackoverflow.com/questions/8945531/pick-any-kind-of-file-via-an-intent-in-android
                    intent.putExtra("CONTENT_TYPE", "*/*");
                }

                startActivityForResult(intent, PICK_VARNAM_CORPUS_FILE);
                return true;
            }
        });
    }

    private void setupVarnamImportVLF() {
        Preference filePicker = findPreference("pref_varnam_import_vlf");

        filePicker.setEnabled(installed);
        if (!installed) {
            return;
        }

        filePicker.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Intent intent = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                    intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                    intent.setType("*/*");
                } else {
                    intent = new Intent(Intent.ACTION_GET_CONTENT);
                    // TODO might not work in some file explorers ?
                    // https://stackoverflow.com/questions/8945531/pick-any-kind-of-file-via-an-intent-in-android
                    intent.putExtra("CONTENT_TYPE", "*/*");
                }

                startActivityForResult(intent, PICK_VARNAM_VLF);
                return true;
            }
        });
    }

    private void setupVarnamExportVLF() {
        Preference dirPicker = findPreference("pref_varnam_export_vlf");

        // Directory picker is not available in older Android version
        if (!installed || Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            dirPicker.setEnabled(false);
            return;
        }
        dirPicker.setEnabled(true);

        dirPicker.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Intent intent = null;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
                } else {
                    intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.setType("*/*");
                }

                startActivityForResult(intent, PICK_VARNAM_EXPORT_DIR);
                return true;
            }
        });
    }

    private String copyUriAndGetPath(Uri uri) throws Exception {
        InputStream in = getActivity().getContentResolver().openInputStream(uri);

        File outFile = File.createTempFile("varnam", "", getActivity().getCacheDir());
        OutputStream out = new FileOutputStream(outFile);

        copyFile(in, out);

        in.close();
        out.close();

        return outFile.getAbsolutePath();
    }

    public static void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while((read = in.read(buffer)) != -1){
            out.write(buffer, 0, read);
        }
    }

    /**
     * Check if a file is gzip format
     * @param path
     * @return
     */
    public static boolean isGZipFile(String path) {
        int magic = 0;
        try {
            RandomAccessFile raf = new RandomAccessFile(path, "r");
            magic = raf.read() & 0xff | ((raf.read() << 8) & 0xff00);
            raf.close();
        } catch (Exception e) {
            Log.e("gzip", e.getMessage());
        }
        return magic == GZIPInputStream.GZIP_MAGIC;
    }

    /**
     * Unzip a file
     * CC-BY-SA 3.0 https://stackoverflow.com/a/10633905/1372424
     * @param stream
     * @param path
     * @return
     */
    private boolean unzip(InputStream stream, String path) {
        try {
            InputStream compressedInputStream = new GZIPInputStream(stream);
            InputSource inputSource = new InputSource(compressedInputStream);
            InputStream inputStream = new BufferedInputStream(inputSource.getByteStream());

            File outputFile = new File(path);
            OutputStream outputStream = new FileOutputStream(outputFile.getAbsoluteFile());

            byte[] buffer = new byte[1024];
            int length;
            while ((length = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, length);
            }

            outputStream.flush();
            outputStream.close();
            inputStream.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
