/*
 * Copyright (C) 2020, Subin Siby <mail@subinsb.com>
 * Licensed under AGPL-3.0-only
 */

package org.smc.inputmethod.indic.varnam;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.preference.PreferenceFragmentCompat;

import org.smc.inputmethod.indic.varnam.R;
import org.smc.inputmethod.indic.varnam.VarnamService;
import org.smc.inputmethod.indic.varnam.VarnamSettingsRLWItem;

import com.varnamproject.govarnam.SchemeDetails;
import com.varnamproject.govarnam.Suggestion;
import com.varnamproject.govarnam.Varnam;

/**
 * "Varnam" language settings sub screen.
 * Allows user to configure varnam languages here
 */
public final class VarnamSettingsRLWFragment extends PreferenceFragmentCompat {
    private SchemeDetails scheme;
    Varnam varnam;

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.prefs_screen_varnam_rlw);

        final Context context = getPreferenceManager().getContext();
        final androidx.preference.PreferenceScreen preferenceScreen = this.getPreferenceScreen();

        String schemeID = getArguments().getString("schemeID");
        scheme = VarnamService.schemes.get(schemeID);
        try {
            varnam = VarnamService.makeVarnam(context, schemeID);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        Suggestion[] words = varnam.getRecentlyLearnedWords(0, 0, 30);

        if (words == null) {
            Log.d("varnam", "RLW fetch failed: " + varnam.getLastError());
            return;
        }

        for (final Suggestion sug : words) {
            final VarnamSettingsRLWItem screen = new VarnamSettingsRLWItem(context);

            screen.setTitle(sug.Word);
            screen.setIconSpaceReserved(false);
            screen.setWidgetLayoutResource(R.layout.unlearn_button);
            screen.setOnUnlearnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        varnam.unlearn(sug.Word);
                        preferenceScreen.removePreference(screen);
                    } catch (Exception e) {
                        Toast.makeText(context, "Couldn't unlearn " + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            });
            preferenceScreen.addPreference(screen);
        }
    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//
//        // This fragment shouldn't need status view
//        LinearLayout status = getActivity().findViewById(R.id.varnam_process_status);
//        status.setVisibility(View.GONE);
//    }
}
