package org.smc.inputmethod.indic.varnam;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.preference.Preference;
import androidx.preference.PreferenceViewHolder;

public class VarnamSettingsProcessInfo extends Preference {
    public VarnamSettingsProcessInfo(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setLayoutResource(R.layout.varnam_process_info);
    }

    public VarnamSettingsProcessInfo(Context context, AttributeSet attrs) {
        super(context, attrs);
        setLayoutResource(R.layout.varnam_process_info);
    }

    public VarnamSettingsProcessInfo(Context context) {
        super(context);
        setLayoutResource(R.layout.varnam_process_info);
    }

    private View view;
    public TextView logTextView;
    public ProgressBar progressBar;

    @Override
    public void onBindViewHolder(PreferenceViewHolder holder) {
        super.onBindViewHolder(holder);
        logTextView = (TextView) holder.findViewById(R.id.varnam_process_log);
        progressBar = (ProgressBar) holder.findViewById(R.id.varnam_process_progress);

        view = holder.itemView;
        setVisibility(View.GONE); // default is hidden
    }

    public void setVisibility(int visibility) {
        view.setVisibility(visibility);
    }

    public int getVisibility() {
        return view.getVisibility();
    }
}
