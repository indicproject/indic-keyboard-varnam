/*
 * Copyright (C) 2020, Subin Siby <mail@subinsb.com>
 * Licensed under AGPL-3.0-only
 */

package org.smc.inputmethod.indic.varnam;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import androidx.preference.CheckBoxPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceManager;

import org.smc.inputmethod.indic.varnam.R;

/**
 * "Varnam" language settings sub screen.
 * Allows user to configure varnam languages here
 */
public final class VarnamSettingsGeneralFragment extends PreferenceFragmentCompat {
    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.prefs_screen_varnam_general);

        final SharedPreferences prefs = getContext().getSharedPreferences("varnam", Context.MODE_PRIVATE);
        boolean learn = prefs.getBoolean("varnam_learn", true);

        final CheckBoxPreference learnCheckbox = findPreference("learn");
        assert learnCheckbox != null;

        learnCheckbox.setChecked(learn);

        learnCheckbox.setOnPreferenceChangeListener(new CheckBoxPreference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(final Preference preference, final Object newValue) {
                prefs.edit().putBoolean("varnam_learn", (boolean) newValue).apply();
                learnCheckbox.setChecked((boolean) newValue);
                return true;
            }
        });
    }
}
