import gzip
import json
from os.path import basename
from pathlib import Path
import shutil
from urllib import request

def copyScheme(schemeID):
    programDir = str(Path(__file__).parent.absolute())
    source = programDir + '/schemes/schemes/' + schemeID
    target = programDir + '/app/src/main/assets'

    packsInfo = []

    for path in Path(source + '/').rglob('*'):
        if basename(path) == schemeID + '.vst':
            shutil.copy2(path, target)
            continue

        for packPath in Path(path).rglob('*'):
            if basename(packPath) == 'pack.json':
                packsInfo.append(json.load(open(packPath, 'r')))
                continue
            
            if ".vlf" not in basename(packPath):
                continue

            with open(
                    packPath, 'rb'
                ) as f_in, gzip.open(
                    target + '/' + basename(packPath).replace('.vlf', '.gzip'),
                    'wb'
                ) as f_out:
                f_out.writelines(f_in)

    with open(target + '/packs.json', 'w') as f:
        json.dump(packsInfo, f, ensure_ascii=False)

# For now just Malayalam for Indic Keyboard
for schemeID in ["ml"]:
    copyScheme(schemeID)
